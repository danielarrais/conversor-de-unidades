import 'package:app_unit_converter/others_clases/unidade.dart';
import 'package:app_unit_converter/pages/converterScreen.dart';
import 'package:app_unit_converter/widgets/backdrop.dart';
import 'package:app_unit_converter/widgets/categoria.dart';
import 'package:app_unit_converter/widgets/categoriaTile.dart';
import 'package:flutter/material.dart';

final _color = Colors.green[100];

class CategoriaScreen extends StatefulWidget {
  const CategoriaScreen();

  @override
  _CategoriaScreenState createState() => _CategoriaScreenState();
}

class _CategoriaScreenState extends State<CategoriaScreen>{


  var categorias = <Categoria>[];
  Categoria _defaultCategory;
  Categoria _currentCategory;

  static const _categoryIcons = <String>[
    'assets/icons/length.png',
    'assets/icons/area.png',
    'assets/icons/volume.png',
    'assets/icons/mass.png',
    'assets/icons/time.png',
    'assets/icons/digital_storage.png',
    'assets/icons/power.png',
    'assets/icons/currency.png',
  ];

  static const _categoryNames = <String>[
    'Tamanho',
    'Área',
    'Volume',
    'Massa',
    'Tempo',
    'Amazenamento',
    'Energia',
    'Moeda',
  ];

  static const _baseColors = <Color>[
    Colors.teal,
    Colors.orange,
    Colors.pinkAccent,
    Colors.blueAccent,
    Colors.yellow,
    Colors.greenAccent,
    Colors.purpleAccent,
    Colors.red,
  ];

  @override
  void initState() {
    super.initState();

    for (var i = 0; i < _categoryNames.length; i++) {
      var categoria = Categoria(titulo: _categoryNames[i], cor: _baseColors[i], icone: _categoryIcons[i], unidades: _criarListaDeUnidade(_categoryNames[i]),);
      
      if (i == 0) {
        _defaultCategory = categoria;
      }

      categorias.add(categoria);
    }
  }

  @override
  Widget build(BuildContext context) {
    final listView = Container(
      color: Colors.grey[50],
      padding: EdgeInsets.only(
        left: 8.0,
        right: 8.0,
        bottom: 48.0,
      ),
      child: _buildListCategorias(),
    );

    return Backdrop(
      currentCategoria:
          _currentCategory == null ? _defaultCategory : _currentCategory,
      frontPanel: _currentCategory == null
          ? ConverterScreen(_defaultCategory)
          : ConverterScreen(_currentCategory),
      backPanel: listView,
      frontTitle: Text('Conversor de unidades'),
      backTitle: Text('Selecione uma categoria'),
    );
  }

  void _onCategoryTap(Categoria category) {
    setState(() {
      _currentCategory = category;
    });
  }

  Widget _buildListCategorias(){
    return ListView.builder(
      itemBuilder: (BuildContext context, int index) {
        return CategoriaTile(
          categoria: categorias[index],
          onTap: _onCategoryTap,
        );
      },
      itemCount: categorias.length,
    );
  }

  /// Returns a list of mock [Unit]s.
  List<Unidade> _criarListaDeUnidade(String nomeCategoria) {
    return List.generate(10, (int i) {
      i += 1;
      return Unidade(
        name: '$nomeCategoria Unidade $i',
        conversion: i.toDouble(),
      );
    });
  }

}