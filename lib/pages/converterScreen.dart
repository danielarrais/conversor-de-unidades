import 'package:app_unit_converter/others_clases/unidade.dart';
import 'package:app_unit_converter/widgets/categoria.dart';
import 'package:flutter/material.dart';

const _padding = EdgeInsets.all(16.0);

class ConverterScreen extends StatefulWidget{

  final Categoria _categoria;

  ConverterScreen(this._categoria);

  @override
  createState() => _StateConverterScreenState();

}

class _StateConverterScreenState extends State<ConverterScreen>{

  Unidade _fromValue;
  Unidade _toValue;
  double _inputValue;
  String _convertedValue = '';
  List<DropdownMenuItem> itens;


  Categoria _categoriaDefault;
  Categoria _categoriaCurrent;

  void _criarItensMenu(){
     var itensCriados = widget._categoria.unidades.map((Unidade u) {
      return DropdownMenuItem(
        value: u.name,
        child: Text(u.name)
      );
    }).toList();

    setState(() {
      itens = itensCriados;
    });
  }

  @override
  void initState() {
    super.initState();
    _criarItensMenu();
    _setDefaults();
  }

  void _setDefaults() {
    setState(() {
      _fromValue = widget._categoria.unidades[0];
      _toValue = widget._categoria.unidades[1];
    });
  }

  Widget _createDropdown(String currentValue, ValueChanged<dynamic> onChanged) {
    return Container(
      margin: EdgeInsets.only(top: 16.0),
      decoration: BoxDecoration(
        // This sets the color of the [DropdownButton] itself
        color: Colors.grey[50],
        border: Border.all(
          color: Colors.grey[400],
          width: 1.0,
        ),
      ),
      padding: EdgeInsets.symmetric(vertical: 8.0),
      child: Theme(
        // This sets the color of the [DropdownMenuItem]
        data: Theme.of(context).copyWith(
              canvasColor: Colors.grey[50],
            ),
        child: DropdownButtonHideUnderline(
          child: ButtonTheme(
            alignedDropdown: true,
            child: DropdownButton(
              value: currentValue,
              items: itens,
              onChanged: onChanged,
              style: Theme.of(context).textTheme.title,
            ),
          ),
        ),
      ),
    );
  }


  /// Clean up conversion; trim trailing zeros, e.g. 5.500 -> 5.5, 10.0 -> 10
  String _format(double conversion) {
    var outputNum = conversion.toStringAsPrecision(7);
    if (outputNum.contains('.') && outputNum.endsWith('0')) {
      var i = outputNum.length - 1;
      while (outputNum[i] == '0') {
        i -= 1;
      }
      outputNum = outputNum.substring(0, i + 1);
    }
    if (outputNum.endsWith('.')) {
      return outputNum.substring(0, outputNum.length - 1);
    }
    return outputNum;
  }

  void _updateConversion() {
    setState(() {
      _convertedValue =
          _format(_inputValue * (_toValue.conversion / _fromValue.conversion));
    });
  }

  void _updateInputValue(String input) {
    setState(() {
      if (input == null || input.isEmpty) {
        _convertedValue = '';
      } else {
        // Even though we are using the numerical keyboard, we still have to check
        // for non-numerical input such as '5..0' or '6 -3'
        try {
          final inputDouble = double.parse(input);
          _inputValue = inputDouble;
          _updateConversion();
        } on Exception catch (e) {
          print('Error: $e');
        }
      }
    });
  }

  Unidade _getUnidade(String unitName) {
    return widget._categoria.unidades.firstWhere(
      (Unidade unit) {
        return unit.name == unitName;
      },
      orElse: null,
    );
  }

  void _updateFromConversion(dynamic unitName) {
    setState(() {
      _fromValue = _getUnidade(unitName);
    });
    if (_inputValue != null) {
      _updateConversion();
    }
  }

  void _updateToConversion(dynamic unitName) {
    setState(() {
      _toValue = _getUnidade(unitName);
    });
    if (_inputValue != null) {
      _updateConversion();
    }
  }
   
  @override
  Widget build(BuildContext context) {
    var input = Container(
      child: Padding(
        padding: _padding,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            TextField(
              decoration: InputDecoration(
                labelStyle: Theme.of(context).textTheme.display1,
                labelText: 'Entrada',
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(0.0),
                ),
              ),
              keyboardType: TextInputType.number,
              onChanged: _updateInputValue,
            ),
            _createDropdown(_fromValue.name, _updateFromConversion)
          ],
        ),
      )
    );

    final arrows = RotatedBox(
      quarterTurns: 1,
      child: Icon(
        Icons.compare_arrows,
        size: 40.0,
      ),
    );

    final output = Padding(
      padding: _padding,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          InputDecorator(
            child: Text(
              _convertedValue,
              style: Theme.of(context).textTheme.display1,
            ),
            decoration: InputDecoration(
              labelText: 'Saída',
              labelStyle: Theme.of(context).textTheme.display1,
              border: OutlineInputBorder(
                borderRadius: BorderRadius.circular(0.0),
              ),
            ),
          ),
          _createDropdown(_toValue.name, _updateToConversion),
        ],
      ),
    );

    final converter = Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: [
        input,
        arrows,
        output,
      ],
    );

    return Padding(
      padding: _padding,
      child: converter,
    );
  }
}