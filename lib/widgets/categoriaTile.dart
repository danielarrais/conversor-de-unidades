import 'package:app_unit_converter/widgets/categoria.dart';
import 'package:flutter/material.dart';
class CategoriaTile extends StatelessWidget{

  final Categoria categoria;
  final ValueChanged<Categoria> onTap;

  const CategoriaTile({
    Key key,
    @required this.categoria,
    @required this.onTap,
  })  : assert(categoria != null),
        assert(onTap != null),
        super(key: key);
  
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Material(
      color: Colors.transparent,
      borderRadius: BorderRadius.all(Radius.circular(50.0)),
      child: Container(
        height: 100,
        padding: EdgeInsets.all(8.0),
        child: InkWell(
          highlightColor: categoria.cor,
          splashColor: categoria.cor,
          onTap: () => onTap(categoria),
          child: Row(
            children: <Widget>[
              _getIcone(),
              _getTextoCategoria(),
            ],
          ),
        ),
      ),
    );
  }
  

  Widget _getIcone(){
    return Padding(
      padding: EdgeInsets.all(16.0),
      child: Image.asset(categoria.icone),
    );
  }

  Widget _getTextoCategoria(){
    return Center(
      child: Text(
          categoria.titulo,
          style: TextStyle(fontSize: 24.0),
        ),
    );
  }
}