import 'package:app_unit_converter/others_clases/unidade.dart';
import 'package:flutter/material.dart';

class Categoria {

  final String titulo;
  final String icone;
  final Color cor;
  final List<Unidade> unidades;

  Categoria({
      Key key, 
      @required String this.titulo, 
      @required Color this.cor, 
      @required String this.icone, 
      @required List<Unidade> this.unidades
    });
}